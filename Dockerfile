# Temporarilty lifted from V Romero at Mulesoft. 
# changes coming to Dockerfile

FROM java:8
MAINTAINER brandon grantham <brandon.grantham@gmail.com>
ARG         MULE_VERSION=3.8.1
RUN cd ~ && wget https://repository-master.mulesoft.org/nexus/content/repositories/releases/org/mule/distributions/mule-standalone/$MULE_VERSION/mule-standalone-$MULE_VERSION.tar.gz --no-check-certificate \	
			&& cd /opt \
			&& tar xvzf ~/mule-standalone-$MULE_VERSION.tar.gz \
			&& rm ~/mule-standalone-$MULE_VERSION.tar.gz \
			&& ln -s /opt/mule-standalone-$MULE_VERSION /opt/mule

# Define environment variables.
ENV MULE_HOME /opt/mule

VOLUME ["/opt/mule/logs", "/opt/mule/conf", "/opt/mule/apps", "/opt/mule/domains"]

# Define working directory.
WORKDIR /opt/mule

CMD [ "/opt/mule/bin/mule" ]

# Default http port
EXPOSE 5000
# Expose Mule JMX port
EXPOSE  1098
# Expose API ports
EXPOSE      8081
EXPOSE 		8082
EXPOSE 		8084
EXPOSE		8085
EXPOSE 		8091
EXPOSE 		8090
CMD [ "/opt/mule/bin/mule" ]
