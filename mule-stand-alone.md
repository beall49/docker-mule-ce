## Run Mule On Premises
 - ### No EE functionality i.e. no data weave, but all things can be replicated in java

### How to get Mule Stand Alone Running
- [Download the Standalone Runtime zip](https://developer.mulesoft.com/download-mule-esb-runtime)
- Prepare the Mule Home
    ```bash 
    $ cd ~/Downloads
    $ sudo mkdir /opt/Mule
    $ sudo unzip anypoint-platform-x.x.x.zip -d /opt/Mule
    ```
- Create MULE_HOME variable
    - put this in your ~/.bash_profile
    ```bash
    MULE_HOME="/opt/Mule/mule-standalone-VERSION.NUMBER.HERE"
    export MULE_HOME
    ```
- Start Mule
    - This will start the mule service
    - It will immediately start any zip that's dropped in the ```bash $MULE_HOME/apps``` folder
    ```bash
    $MULE_HOME/bin/mule start 
    ```

- To start a specific app
    ```bash
    $MULE_HOME/bin/mule -app first-app-name:another-app-name
    ```
    - To add an app, just drop the zip in the ```bash $MULE_HOME/apps``` folder

#### Mule Docker Docs
- [https://docs.mulesoft.com/anypoint-private-cloud/v/1.1.0/installing-anypoint-on-premises](https://docs.mulesoft.com/anypoint-private-cloud/v/1.1.0/installing-anypoint-on-premises)

#### Running an app locally
- [https://docs.mulesoft.com/mule-user-guide/v/3.7/application-deployment](https://docs.mulesoft.com/mule-user-guide/v/3.7/application-deployment)

#### [Brandons Mule Docker Image](https://hub.docker.com/r/granthbr/api-gateway/)
- [Brandon Git Hub Repo ready to clone CE (check mule rt version)](https://github.com/granthbr/mule-ce) 
- You can use this to run on prem in docker
- You'll need to make a sim link between the host machine and the docker image for where the apps live