# Mule Community edition container
## [Sto....errr forked from the Brandon](https://github.com/granthbr/mule-ce)

1. This is for mule version 3.8.1 If you want to change the version update the version in the dockerfile.
> MULE_VERSION=3.8.1

2. After cloning, cd into docker-mule-ce

```bash
cd /docker-mule-ce
```

3. Build it, it can take a while
```bash
docker build -t docker-mule . 
```

4. Drop a zip into the ```/apps``` folder	

5. Run the image
```bash
docker run -d -v $PWD/conf/:/opt/mule/conf -v $PWD/logs:/opt/mule/logs -v $PWD/apps:/opt/mule/apps -p 8082:8082 docker-mule
```





